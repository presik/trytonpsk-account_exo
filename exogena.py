# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
# -*- coding: utf-8 -*-
from datetime import date
from decimal import Decimal

from sql.operators import NotIn
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

from .exceptions import ExoRuleError

_ZERO = Decimal(0)

DEFINITIONS = {
    '': {},
    '1001': {
        '1001_pagoded': 'Pago Deducible',
        '1001_pagonoded': 'Pago No Deducible',
        '1001_ivaded': 'IVA Deducible',
        '1001_ivanoded': 'IVA No Deducible',
        '1001_retprac': 'Retenciones Practicadas',
        '1001_retasum': 'Retenciones Asumidas',
        '1001_retpracivarc': 'Retenfte Pract. IVA RC',
        '1001_retasumivars': 'Retenfte Pract. IVA RS',
        '1001_retpracivanodo': 'Retenfte Pract. IVA ND',
        '1001_retpracree': 'Retenfte Pract. CREE',
        '1001_retasumcree': 'Retenfte Asum. CREE',
    },
    '1003': {
        '1003_pagosujeret': 'Valor acumulado del pago o abono sujeto a Retencion en la fuente.',
        '1003_retpract': 'Retencion que le Practicaron',
    },
    '1005': {
        '': '',
        '1005_impdescon': 'Impuesto Descontable.',
        '1005_ivadev': 'IVA resultante por devoluciones en ventas anuladas. rescindidas o resueltas',
    },
    '1006': {
        '1006_impgen': 'Impuesto generado',
        '1006_ivarecup': 'IVA recuperado en devoluciones en compras anuladas, rescindidas o resueltas',
        '1006_impoconsumo': 'Impuesto Consumo',
    },
    '1007': {
        '1007_ingbrutprop': 'Ingresos brutos recibidos por operaciones propias.',
        '1007_ingbruconsorut': 'Ingresos brutos a traves de Consorcio o Uniones Temporales',
        '1007_ingcontraadm': 'Ingresos a traves de Contratos de mandato o administracion delegada',
        '1007_ingexplo': 'Ingresos a traves de exploracion y explotacion de minerales',
        '1007_ingfidu': 'Ingresos a traves de fiducia',
        '1007_ingtercer': 'Ingresos recibidos a traves de terceros',
        '1007_ingdev': 'Devoluciones, rebajas y descuentos',
    },
    '1008': {
        '1008_saldocxc': 'Saldo cuentas por cobrar al 31-12.',
    },
    '1009': {
        '1009_saldocxp': 'Saldo cuentas por pagar al 31-12.',
    },
    '1011': {
        '1011_saldo': 'Saldo cuentas al 31-12.',
    },
    '1012': {
        '1012_saldo': 'Saldo cuentas al 31-12.',
    },
    '1043': {
        '1043_pago_abono': 'Valor del pago o abono en cuenta',
        '1043_ivamayor_valor_costo': 'Iva mayor valor al costo',
        '1043_retencion_prac': 'Retención en la fuente practicada',
        '1043_retencion_asu': 'Valor de retención en la fuente asumida',
        '1043_reteiva_regcomun': 'Retención Iva practicada Régimen Común',
        '1043_retencion_regsim': 'Retención asumida Régimen Simplificado',
        '1043_retencion_asum_no_dom': 'Retención en la fuente asumida No domicialiados',
        '1043_retencion_cree': 'Retención en la fuente practicadas CREE',
        '1043_retencion_cree_asum': 'Retención en la fuente asumidas CREE',
    },
    '1045': {
        '1045_ingresos_brutos_rec': 'Ingresos Brutos Recibidos',
        '1045_dev_rebajas': 'Devoluciones y rebajas',
    },
    '2015': {
        '2015_valor': 'Valor base la retencion',
        '2015_ret': 'Retencion',
    },
    '5247': {
        '5247_pago_abono': 'Valor del pago o abono en cuenta',
        '5247_ivamayor_valor_costo': 'Iva mayor valor al costo',
        '5247_retencion_prac': 'Retención en la fuente practicada',
        '5247_retencion_asu': 'Valor de retención en la fuente asumida',
        '5247_reteiva_regcomun': 'Retención Iva practicada Régimen Común',
        '5247_retencion_asum_no_dom': 'Retención en la fuente asumida No domicialiados',
    },
    '5248': {
        '5248_ingresos_brutos': 'Valor de ingresos brutos recibidos',
        '5248_dev_descuentos': 'Devoluciones, rebajas y descuentos',
    },
    '5249': {
        '5249_iva_descontable': 'Valor de Iva descontable',
        '5249_iva_devoluciones': 'Iva resultante por devoluciones aplicadas',
    },
    '5250': {
        '5250_iva_generado': 'Valor Ivan generado',
        '5250_iva_rec_devoluciones': 'Iva recuperado en devoluciones Devoluciones',
        '5250_impuesto_consumo': 'Impuesto al consumo',
    },
    '5251': {
        '5251_saldo_anual': 'Saldo de cuentas a 31-12',
    },
    '5252': {
        '5252_saldo_anual': 'Saldo de cuentas a 31-12',
    },
    '2276': {
    },
}

CUANTIAS_MENORES = 'Cuantias menores'
TIPO_DOC_CUANTIAS_MENORES = '43'
NIT_CUANTIAS_MENORES = '222222222'
VALOR_CUANTIAS_MENORES = Decimal(100000)

TIPO_DOC_EXTERIOR_VAT_NUMBER = '42'
TIPO_DOC_EXTERIOR_NO_VAT_NUMBER = '43'
INICIO_EXTRANJERO_VAT_NUMBER = 444444001


class Account(metaclass=PoolMeta):
    __name__ = 'account.account'
    codigo_exo = fields.Integer('Codigo Exogena')


class ExogenaConcepto(ModelSQL, ModelView):
    "Exogena Concepto"
    __name__ = 'account.exogena_concepto'
    report = fields.Selection('get_report', 'Report')
    concept = fields.Char('Concept', states={
            'readonly': Eval('distribute_to_peer', False),
        })
    account_code = fields.Char('Account Code')
    account = fields.Many2One('account.account', 'Account')
    definition = fields.Selection('selection_definition', 'Definition',
        depends=['report'])
    distribute_to_peer = fields.Boolean('Distribute by Peer',
        help='This concept is not registered independently, instead is \
            regitered with peer accounts on moves')
    rate_not_deducible = fields.Numeric('Rate not Deducible')
    nature_account = fields.Selection([
            ('', ''),
            ('debit', 'Debit'),
            ('credit', 'Credit'),
    ], 'Nature Account')
    exclude_reconciled = fields.Boolean('Exclude Reconciled')
    restar_debit_credit = fields.Boolean('Restar Debitos-Creditos')
    company = fields.Many2One('company.company', 'Company', required=True,
            ondelete="RESTRICT")

    @classmethod
    def __setup__(cls):
        super(ExogenaConcepto, cls).__setup__()

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or None

    @classmethod
    def get_report(cls):
        return [(str(d), str(d)) for d in DEFINITIONS]

    @fields.depends('report')
    def selection_definition(self):
        res = [('', '')]
        if self.report:
            res.extend(DEFINITIONS[self.report].items())
        return res

    @fields.depends('definition')
    def on_change_report(self):
        self.definition = ('', None)

    @fields.depends('concept', 'distribute_to_peer')
    def on_change_distribute_to_peer(self):
        if self.distribute_to_peer:
            self.concept = '0000'
        else:
            self.concept = ''


class ExogenaCodigoConcepto(ModelSQL, ModelView):
    "Exogena Codigo Concepto"
    __name__ = 'account.exogena_codigo_concepto'
    _rec_name = 'code'
    code = fields.Char('Code', required=True)
    name = fields.Char('Concept')


class SynchronizeAccountsStart(ModelView):
    "Synchronize Accounts Start"
    __name__ = 'account.exo.synchronize.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True)


class SynchronizeAccounts(Wizard):
    "Synchronize Accounts"
    __name__ = 'account.exo.synchronize'
    start = StateView('account.exo.synchronize.start',
        'account_exo.synchronize_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'do_synchronize', 'tryton-ok', default=True),
        ])
    do_synchronize = StateTransition()

    def transition_do_synchronize(self):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Move = pool.get('account.move')
        Account = pool.get('account.account')
        Company = pool.get('company.company')
        Period = pool.get('account.period')
        FiscalYear = pool.get('account.fiscalyear')
        Concept = pool.get('account.exogena_concepto')
        context = Transaction().context
        cursor = Transaction().connection.cursor()

        company_id = context.get('company')
        company = Company(company_id)

        line = MoveLine.__table__()
        move = Move.__table__()
        period = Period.__table__()
        fiscalyear = FiscalYear.__table__()

        concepts = Concept.search([])
        if concepts:
            current_accounts = [c.account.id for c in concepts if c.account]
        else:
            current_accounts = []

        where = ((period.fiscalyear == self.start.fiscalyear.id)
            & (move.company == company.id))

        if current_accounts:
            where &= NotIn(line.account, current_accounts)
        query = line.join(move, 'LEFT', condition=line.move == move.id,
                ).join(period, 'LEFT', condition=move.period == period.id,
                ).join(fiscalyear, 'LEFT',
                    condition=period.fiscalyear == fiscalyear.id,
                ).select(line.account,
                    where=where,
                    group_by=line.account)
        cursor.execute(*query)

        result = cursor.fetchall()
        accounts_used = [row[0] for row in result]

        accounts = Account.browse(accounts_used)
        concepts_to_create = []

        for acc in accounts:
            concepts_to_create.append({
                'account': acc.id,
                'account_code': acc.code,
            })
        Concept.create(concepts_to_create)
        return 'end'


class ExogenaDefinitionAccountStart(ModelView):
    "Exogena Definition Account Start"
    __name__ = 'account_exo.exogena_definition_account.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    report = fields.Selection('get_report', 'Report', required=True)
    codigo_concepto = fields.Char('Codigo Concepto', required=True)
    definition = fields.Selection('selection_definition', 'Definition',
        required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def get_report(cls):
        return [(d, d) for d in DEFINITIONS]

    @fields.depends('report')
    def selection_definition(self):
        res = []
        if self.report:
            res.extend(DEFINITIONS[self.report].items())
        return res


class ExogenaDefinitionAccount(Wizard):
    "Exogena Definition Account"
    __name__ = 'account_exo.exogena_definition_account'
    start = StateView('account_exo.exogena_definition_account.start',
        'account_exo.exogena_definition_account_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Accept', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        Concept = Pool().get('account.exogena_concepto')
        ids_ = Transaction().context.get('active_ids')
        concepts = Concept.browse(ids_)
        Concept.write(concepts, {
            'report': self.start.report,
            'definition': self.start.definition,
            'concept': self.start.codigo_concepto,
        })
        return 'end'


class PrintReportExogenaStart(ModelView):
    "Print Report Exogena Start"
    __name__ = 'account_exo.print_report_exogena.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    start_period = fields.Many2One('account.period', 'Start Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('start_date', '<=', (Eval('end_period'), 'start_date')),
            ('type', '=', 'standard'),
            ], required=True,
        depends=['fiscalyear', 'end_period'])
    end_period = fields.Many2One('account.period', 'End Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('start_date', '>=', (Eval('start_period'), 'start_date')),
            ('type', '=', 'standard'),
            ], required=True,
        depends=['fiscalyear', 'start_period'])
    company = fields.Many2One('company.company', 'Company', required=True)
    report = fields.Many2One('ir.action.report', 'Report',
            domain=[
                ('module', '=', 'account_exo'),
                ('report_name', '!=', 'account.f0000'),
                ], required=True)

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), test_state=False).id

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('fiscalyear', 'start_period', 'end_period')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None


class PrintReportExogena(Wizard):
    "Print Report Exogena"
    __name__ = 'account_exo.print_report_exogena'
    start = StateView('account_exo.print_report_exogena.start',
        'account_exo.print_report_exogena_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('account.f0000')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'start_period': self.start.start_period.id,
            'end_period': self.start.end_period.id,
            'report': self.start.report.id,
        }

        action['report'] = self.start.report.report
        action['report_name'] = self.start.report.report_name
        action['id'] = self.start.report.id
        action['action'] = self.start.report.action.id
        return action, data

    def transition_print_(self):
        return 'end'


# REPORTS  ##############################

class F0000(Report):
    "0000 Template"
    __name__ = 'account.f0000'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        """ Example
        {
        '5001': {
            'juan': {'a': 3, 'b': 7, 'c': 1},
            'pedro': {'a': 16, 'b':12, 'c': 10},
            },
        '5003': {
            'juan': ['a': 4, 'b': 11, 'c': 3],
            'maria': ['r': 5, 'w': 20, 'n': 0],
            'pedro': ['x': 21, 'y': 8, 'z': 2],
            },
        }
        """

        pool = Pool()
        Concept = pool.get('account.exogena_concepto')
        Line = pool.get('account.move.line')
        Period = pool.get('account.period')
        Company = pool.get('company.company')
        LineTax = pool.get('account.tax.line')
        Party = pool.get('party.party')
        cursor = Transaction().connection.cursor()
        parties = Party.search_read([
            ('id_number', 'ilike', '222222%'),
        ])
        parties_ = ''
        if parties:
            parties = tuple(p['id'] for p in parties)
            parties = str(parties).replace(',', '') if len(parties) == 1 else str(parties)
            parties_ = 'AND party NOT IN %s' % parties

        report_number = cls.__name__[-4:]
        fields_concepts = [
            'definition', 'concept', 'distribute_to_peer',
            'rate_not_deducible', 'nature_account',
            'exclude_reconciled', 'restar_debit_credit',
            'account.name',
        ]
        concepts = Concept.search_read([
            ('report', '=', report_number),
            ('concept', '!=', ''),
            ('definition', '!=', ''),
        ], fields_names=fields_concepts)

        accounts_target = {}
        for c in concepts:
            if not c['definition'] or not c['concept']:
                raise ExoRuleError(
                    gettext('account_exo.msg_concept_empty', s=c['account.']['name']),
                )
            accounts_target[c['id']] = {
                'definition': c['definition'],
                'concept': c['concept'],
                'dtp': c['distribute_to_peer'],
                'rate_not_deducible': c['rate_not_deducible'],
                'nature_account': c['nature_account'],
                'exclude_reconciled': c['exclude_reconciled'],
                'restar_debit_credit': c['restar_debit_credit'],
            }

        records = {}

        definitions = DEFINITIONS[report_number].copy()

        start_period = Period(data['start_period'])
        end_period = Period(data['end_period'])
        data['start_date'] = start_period.start_date
        data['end_date'] = end_period.end_date
        if report_number in ('1009', '1008'):
            periods = Period.search([
                ('start_date', '<=', end_period.start_date),
                ('type', '=', 'standard'),
            ])
        else:
            periods = Period.search([
                ('fiscalyear', '=', data['fiscalyear']),
                ('start_date', '>=', start_period.start_date),
                ('start_date', '<=', end_period.start_date),
                ('type', '=', 'standard'),
            ])

        period_ids = tuple(p.id for p in periods)
        if len(period_ids) == 1:
            period_ids = str(period_ids).replace(',', '')
        list_parties = set()
        for c in concepts:
            c_id = c['id']
            query = f"""SELECT ml.id, ml.account, ml.party, ml.debit, ml.credit, ml.move
                FROM account_move_line AS ml
                JOIN account_move AS m ON ml.move=m.id
                WHERE account={c['account.']['id']} AND m.period IN {period_ids}
                AND ml.party IS NOT NULL {parties_}"""
            cursor.execute(query)
            result = cursor.fetchall()
            # dom_lines = [
            #     ('account', '='),
            #     ('move.period', 'in', period_ids),
            #     ('party', '!=', None),
            # ]
            # if cliente_pos:
            #     dom_lines.extend([
            #         ('party', '!=', cliente_pos),
            #     ])

            # lines = Line.search_read(dom_lines)
            taxes = {}
            if result:
                line_ids = tuple(r[0] for r in result)
                if len(line_ids) == 1:
                    line_ids = str(line_ids).replace(',', '')
                query_tax = f"""SELECT t.move_line, t.tax, ct.rate
                    FROM account_tax_line AS t
                    JOIN account_tax AS ct ON t.tax=ct.id
                    WHERE t.move_line IN {line_ids}"""
                cursor.execute(query_tax)
                result_tax = cursor.fetchall()
                taxes = {r[0]: r[2] for r in result_tax}

            for line in result:
                id_, account, party, debit, credit, move = line
                list_parties.add(party)
                # lines_taxes = LineTax.search([
                #     ('move_line.id', '=', line.id),
                # ])
                base = 0
                tax_rate = taxes.get(id_)
                if tax_rate and tax_rate != 0:
                    amount_tax = abs(debit + credit)
                    # if lines_taxes[0].tax and lines_taxes[0].tax.rate and lines_taxes[0].tax.rate != 0:
                    base = amount_tax / tax_rate

                is_dtp = accounts_target[c_id]['dtp']
                definition = accounts_target[c_id]['definition']
                concept_exo = accounts_target[c_id]['concept']
                rate_not_deducible = accounts_target[c_id]['rate_not_deducible']
                nature_account = accounts_target[c_id]['nature_account']
                exclude_reconciled = accounts_target[c_id]['exclude_reconciled']
                restar_debit_credit = accounts_target[c_id]['restar_debit_credit']

                # side, otherside = line.account.type.display_balance.split('-')
                # Ignore line reconciled counterpart because it not sums value twice
                # adding to black list reconciled (reconciliation, debit-credit)
                # party = Party(party)
                # if party.type_person != 'persona_natural':
                #     party.first_name = None
                #     party.second_name = None
                #     party.first_family_name = None
                #     party.second_family_name = None
                # else:
                #     party.name = None
                if nature_account == 'debit':
                    if restar_debit_credit:
                        value = debit - credit
                    else:
                        if debit <= 0:
                            continue
                        value = debit
                elif nature_account == 'credit':
                    if restar_debit_credit:
                        value = credit - debit
                    else:
                        if credit <= 0:
                            continue
                        value = credit
                else:
                    value = debit - credit
                # The objective this sentence is change concept_exo target
                # because account dtp is must be assigned to another concept
                if is_dtp:
                    query_dtp = f"""
                        SELECT ml.account, ml.debit, ml.credit
                        FROM account_move_line AS ml
                        JOIN account_move AS m ON ml.move=m.id
                        WHERE move={move}
                    """
                    cursor.execute(query_dtp)
                    result_dtp = cursor.fetchall()
                    for ml in result_dtp:
                        dtp_account, dtp_debit, dtp_credit = ml
                        if dtp_account == account:
                            continue
                        if (dtp_debit > _ZERO and debit > _ZERO) or (dtp_credit > _ZERO and credit > _ZERO):
                            peer_account = accounts_target.get(dtp_account)
                            if peer_account:
                                concept_exo = peer_account['concept']
                                break

                definitions['base'] = 0
                records.setdefault(concept_exo, {})
                records[concept_exo].setdefault(
                    party, {}.fromkeys(definitions.keys(), Decimal(0)),
                )
                if rate_not_deducible and report_number == '1001':
                    deducible = value * (100 - rate_not_deducible) / 100
                    not_deducible = value - deducible
                    if report_number == '1001':
                        if 'iva' in definition:
                            definition_not_ded = '1001_ivanoded'
                        else:
                            definition_not_ded = '1001_pagonoded'
                        records[concept_exo][party][definition_not_ded] += int(not_deducible)
                        value = deducible
                    else:
                        value = not_deducible
                records[concept_exo][party][definition] += int(value)
                if base != 0:
                    records[concept_exo][party]['base'] += abs(int(base))

        fields_party = ['type_document', 'id_number', 'check_digit', 'first_family_name',
            'second_family_name', 'first_name', 'second_name', 'type_person',
            'name', 'country.name', 'country.dian_code', 'city_co.name', 'city_co.dian_code',
            'subdivision.name', 'subdivision.dian_code', 'street', 'addresses.street',
            'email', 'phone', 'mobile', 'ciiu_code',
            ]
        parties = Party.search_read([
            ('id', 'in', list(list_parties)),
            ('active', 'in', [True, False]),
            ], fields_names=fields_party)
        dict_parties = {p['id']: p for p in parties}
        data['today'] = date.today()
        company = Company(data['company'])
        city = ''
        subdivision = ''
        if company.party.city_co:
            city = company.party.city_co
        if company.party.subdivision:
            subdivision = company.party.subdivision
        if not records:
            raise UserError('No se encontraron registros para este reporte')
        data['city'] = city
        data['department'] = subdivision
        report_context['records'] = records.items()
        report_context['dict_parties'] = dict_parties
        return report_context


class F1001(F0000):
    "1001 Pagos o abonos en cuenta y retenciones practicadas"
    __name__ = 'account.f1001'


class F1003(F0000):
    "1003 Retenciones en la fuente que le practicaron"
    __name__ = 'account.f1003'


class F1005(F0000):
    "1005 Impuesto a las ventas por pagar (Descontable)"
    __name__ = 'account.f1005'


class F1006(F0000):
    "1006 Impuesto a las ventas por pagar (Generado)"
    __name__ = 'account.f1006'


class F1007(F0000):
    "1007 Ingresos Recibidos"
    __name__ = 'account.f1007'


class F1008(F0000):
    "1008 Saldo de cuentas por cobrar"
    __name__ = 'account.f1008'


class F1009(F0000):
    "1009 Saldo de cuentas por Pagar"
    __name__ = 'account.f1009'


class F1011(F0000):
    "1011 Información de las declaraciones Tributarias"
    __name__ = 'account.f1011'


class F1012(F0000):
    "1012 Información de declaraciones tributarias, acciones, inversiones en bonos títulos valores y cuentas de ahorro y cuentas corrientes "
    __name__ = 'account.f1012'


class F2276(F0000):
    "2276 Información Certificado de Ingresos y Retenciones para Personas Naturales Empleados"
    __name__ = 'account.f2276'


class F2015(F0000):
    "2015 Retenciones"
    __name__ = 'account.f2015'


class F5247(F0000):
    "5247 Pagos o abonos en cuenta y retenciones practicadas en contratos de colaboración empresarial"
    __name__ = 'account.f5247'


class F5248(F0000):
    "5248 Ingresos recibidos en contratos de colaboración empresarial"
    __name__ = 'account.f5248'


class F5249(F0000):
    "5249 IVA descontable en contratos de colaboración empresarial"
    __name__ = 'account.f5249'


class F5250(F0000):
    "5250 IVA generado en contratos de colaboración empresarial"
    __name__ = 'account.f5250'


class F5251(F0000):
    "5251 Saldos cuentas por cobrar a 31 de diciembre en contratos de colaboración empresarial"
    __name__ = 'account.f5251'


class F5252(F0000):
    "5252 Saldos de cuentas por pagar al 31 de diciembre en contratos de colaboración empresarial"
    __name__ = 'account.f5252'


class F1043(F0000):
    "1043 Pagos o Abonos en cuenta y retenciones practicadas de consorcio y uniones temporales"
    __name__ = 'account.f1043'


class F1045(F0000):
    "1045 Información de ingresos recibidos por consorcios y uniones temporales"
    __name__ = 'account.f1045'


class ExoTerceros(Report):
    "Terceros Información de las declaraciones Tributarias"
    __name__ = 'account.exo.terceros'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Party = Pool().get('party.party')
        report_context['records'] = Party.search([])
        return report_context
