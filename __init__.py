# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import exogena


def register():
    Pool.register(
        exogena.ExogenaCodigoConcepto,
        exogena.ExogenaConcepto,
        exogena.Account,
        exogena.PrintReportExogenaStart,
        exogena.ExogenaDefinitionAccountStart,
        exogena.SynchronizeAccountsStart,
        module='account_exo', type_='model')
    Pool.register(
        exogena.PrintReportExogena,
        exogena.SynchronizeAccounts,
        exogena.ExogenaDefinitionAccount,
        module='account_exo', type_='wizard')
    Pool.register(
        exogena.F0000,
        exogena.F1001,
        exogena.F1003,
        exogena.F1005,
        exogena.F1006,
        exogena.F1007,
        exogena.F1008,
        exogena.F1009,
        exogena.F1011,
        exogena.F1012,
        exogena.F2015,
        exogena.F5247,
        exogena.F5248,
        exogena.F5249,
        exogena.F5250,
        exogena.F5251,
        exogena.F5252,
        exogena.F1043,
        exogena.F1045,
        exogena.F2276,
        exogena.ExoTerceros,
        module='account_exo', type_='report')
